%global         lua_version             5.4
%{!?lua_pkgdir: %global     lua_pkgdir  %{_datadir}/lua/%{lua_version}}

Name:           luarocks
Version:        3.9.2
Release:        1
Summary:        A deployment and management system for Lua modules

License:        MIT
URL:            http://luarocks.org
Source0:        http://luarocks.org/releases/luarocks-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  make
BuildRequires:	lua-devel

Requires:       unzip
Requires:       zip
Requires:	lua


%description
LuaRocks allows you to install Lua modules as self-contained packages
called "rocks", which also contain version dependency
information. This information is used both during installation, so
that when one rock is requested all rocks it depends on are installed
as well, and at run time, so that when a module is required, the
correct version is loaded. LuaRocks supports both local and remote
repositories, and multiple local rocks trees.


%prep
%autosetup -p1


%build
./configure \
  --prefix=%{_prefix} \
  --lua-version=%{lua_version}
%make_build

%install
%make_install

mkdir -p %{buildroot}%{_prefix}/lib/luarocks/rocks-%{lua_version}

%check
# Need a offline regression test 
# ./smoke_test.sh


%files
%license COPYING
%doc README.md
%dir %{_sysconfdir}/luarocks
%config(noreplace) %{_sysconfdir}/luarocks/config-%{lua_version}.lua
%{_bindir}/luarocks
%{_bindir}/luarocks-admin
%{_prefix}/lib/luarocks
%{lua_pkgdir}/luarocks


%changelog
* Tue Aug 1 2023 Xinwei Hu <shinwell_hu@openeuler.sh> - 3.9.2-1
- Decouple luarocks from openresty
- Upgrade to 3.9.2
  . Improved connectivity resiliency
  . A couple of minor feature additions

* Thu Jan 5 2023 Wenlong Zhang<zhangwenlong@loongson.cn> - 3.5.0-3
- fix build error for loongarch64

* Thu Mar 24 2022 liyanan <liyanan32@huawei.com> - 3.5.0-2
- remove %dist

* Fri Jul 30 2021 Joey Ma <majunjie@apache.org> - 3.5.0-1
- Initial package with 3.5.0, which only suppports openresty luajit for now
